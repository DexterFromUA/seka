import React from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";

import { uiConfig, fbAuth } from "../../config/firebase";

const PhoneAuth = () => {
  return (
    <div>
      <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={fbAuth} />
    </div>
  );
};

export default PhoneAuth;
