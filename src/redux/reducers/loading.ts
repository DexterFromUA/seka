import { createReducer } from "@reduxjs/toolkit";
import { setLoadingAction } from "../actions/loading";

const loadingReducer = createReducer(
  {},
  {
    [setLoadingAction.type]: (_, action) => action.payload,
  },
  [],
  (state) => state
);

export default loadingReducer;
