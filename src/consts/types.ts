// Screen types
export interface TInitialScreen {
  user: Object;
  setUser: (user: Object) => void;
  loading: boolean;
  setLoading: (loading: boolean) => void;
}

export interface TLoginScreen {
  getUserAction: () => Record<string, any>;
}

export interface TSettingScreen {
  user: any;
}

// Component types
export interface TPhoneAuth {
  setUser: (user: Object) => void;
}

// HOC types
export interface IWithLayout {
  children: any;
}
