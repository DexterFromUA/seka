import { HOME, SETTING } from "../consts/routes";
import HomeScreen from "../screens/HomeScreen";
import SettingScreen from "../screens/SettingScreen";

export default [
  {
    path: HOME,
    component: HomeScreen,
  },
  {
    path: SETTING,
    component: SettingScreen,
  },
];
