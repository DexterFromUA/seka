import React from "react";
import { Dispatch } from "redux";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { fbAuth } from "../../config/firebase";
import { TInitialScreen } from "../../consts/types";
import { setUserAction } from "../../redux/actions/user";
import { setLoadingAction } from "../../redux/actions/loading";
import LoginScreen from "../LoginScreen";
import Loading from "../../components/ui/Loading";
import Router from "../../router";
import "./index.scss";

const InitialScreen = ({
  user,
  setUser,
  loading,
  setLoading,
}: TInitialScreen) => {
  React.useEffect(() => {
    setLoading(true);
    const unregisterAuthObserver = fbAuth.onAuthStateChanged((user) => {
      if (!!user) {
        let userData = {
          uid: user.uid,
          displayName: user.displayName,
        };
        setUser(userData);
      }
      setLoading(false);
    });

    return () => {
      unregisterAuthObserver();
    };
  }, [setUser, setLoading]);

  if (loading) {
    return <Loading />;
  } else {
    return Object.keys(user).length > 0 ? <Router /> : <LoginScreen />;
  }
};

const mapStateToProps = (state: Record<string, any>) => {
  return {
    user: state.user,
    loading: state.loading,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return bindActionCreators(
    {
      setUser: setUserAction,
      setLoading: setLoadingAction,
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(InitialScreen);
