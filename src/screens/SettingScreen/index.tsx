import React from "react";
import { connect } from "react-redux";

import { TSettingScreen } from "../../consts/types";
import { fbAuth } from "../../config/firebase";

const SettingScreen = ({ user }: TSettingScreen) => {
  const [name, setName] = React.useState("");

  const setNickname = () => {
    console.log("WORK");
    fbAuth.currentUser?.updateProfile({
      displayName: name,
    });
  };

  const checkNickname = () => {
    const cu = fbAuth.currentUser;
    console.log("current user:", cu);
  };

  return (
    <div>
      <input
        type="text"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <button onClick={setNickname}>set</button>
      <button onClick={checkNickname}>check</button>
    </div>
  );
};

const mapStateToProps = (state: Record<string, any>) => {
  return {
    user: state.user,
  };
};

export default connect(mapStateToProps)(SettingScreen);
