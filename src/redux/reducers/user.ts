import { createReducer } from "@reduxjs/toolkit";
import { setUserAction } from "../actions/user";

const userReducer = createReducer(
  {}, // initial state
  {
    [setUserAction.type]: (state, action) => action.payload,
  }, // reducers
  [], // matchers
  (state) => state // default
);

export default userReducer;
