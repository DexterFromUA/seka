import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import "./index.scss";
import "jquery/dist/jquery.min.js";
import "bootstrap/dist/js/bootstrap.min.js";

import store from "./redux/store";
import InitialScreen from "./screens/InitialScreen";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <InitialScreen />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
