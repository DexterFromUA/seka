import { createAction } from "@reduxjs/toolkit";
import { SET_LOADING } from "../../consts/actions";

export const setLoadingAction = createAction(
  SET_LOADING,
  (payload: boolean) => {
    return { payload };
  }
);
