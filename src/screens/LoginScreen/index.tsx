import React from "react";
import PhoneAuth from '../../components/core/phoneAuth';

import "./index.scss";

const LoginScreen = () => {
  return (
    <div className="container">
      <PhoneAuth />
    </div>
  );
};

export default LoginScreen;
