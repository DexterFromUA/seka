import { createAction } from "@reduxjs/toolkit";
import { SET_USER } from "../../consts/actions";

export const setUserAction = createAction(SET_USER, (payload: any) => {
  return {
    payload,
  };
});
