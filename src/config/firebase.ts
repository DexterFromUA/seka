import firebase from "firebase";
import cfg from "../protected/firebase.json";

const uiConfig = {
  signInFlow: "popup",
  signInOptions: [
    {
      provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
      recaptchaParameters: {
        size: "invisible",
      },
    },
  ],
  callbacks: {
    signInSuccessWithAuthResult: (authResult: any, url: string) => {
      console.log(JSON.stringify(authResult.user));
      console.log(authResult);
      return false;
    },
  },
};
// firebase.analytics();
const fb = firebase.initializeApp(cfg);
const fbAuth = firebase.auth();
firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL);
firebase.auth().languageCode = "ru";
const fbDb = firebase.database();

export { fb, uiConfig, fbAuth, fbDb };
