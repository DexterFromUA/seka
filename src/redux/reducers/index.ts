import { combineReducers } from "redux";

import userReducer from "./user";
import loadingReducer from "./loading";

const rootReducer = combineReducers({
  loading: loadingReducer,
  user: userReducer,
});

export default rootReducer;
