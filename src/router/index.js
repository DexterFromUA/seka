import React from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";

import Layout from "../components/layout";
import routes from "./routes";

const Router = () => {
  const routeList = routes.map((route, idx) => {
    return (
      <Route
        key={idx}
        exact
        path={route.path}
        component={(props) => <route.component {...props} />}
      />
    );
  });

  return (
    <BrowserRouter>
      <Layout>
        <Switch>{routeList}</Switch>
      </Layout>
    </BrowserRouter>
  );
};

export default Router;
